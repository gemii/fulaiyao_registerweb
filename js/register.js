/**
 * Created by jiayi.hu on 10/21/16.
 */
var code;
var userInfo_reply;
var openID, GroupID, aid, an, backurl;
var cookie_openID;
var province_val;
var userInfoArray = new Array();
//数据下拉处理
function dataload(filename) {
    var arraydata;
    $.ajax({
        type: "GET",
        url: filename,
        dataType: "json",
        async: false,
        success: function (json) {
            arraydata = eval(json)
        },
        error: function () {

        }
    });
    return arraydata;
}

//解析url得到code
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return (r[2]);
    return null;
}
//解析URL得到所有的参数
function GetUrlParms() {
    var parameterArray = new Array();
    var pairs = window.location.search.substr(1).split("&");
    $.each(pairs, function (k, v) {
        parameterArray.push({"name": v.substring(0).split("=")[0], "value": v.substring(0).split("=")[1]});
    })
    return parameterArray;
}
function get_province(obj) {
    province_val = obj.value;
    if (province_val == -1) {
        $("#provinceSelect>span").text("选择省");
    } else {
        $("#provinceSelect>span").text($("option[value='" + province_val + "']").text());
        $("#provinceSelect>span").attr("id", province_val);
        get_cityOption();
    }
}
function get_city(obj) {
    var val = obj.value;
    if (val == -1) {
        $("#citySelect>span").text("选择市");
    } else {
        $("#citySelect>span").text($("option[value='" + val + "']").text());
    }
}
function get_cityOption() {
    var dataCity = dataload("json/citys.json");
    var cities = new Array();
    var val = $("#provinceSelect>span").get(0).id;
    if (val == "") {
        cities.splice(0, cities.length);
        $.each(dataCity.city_object, function (k, v) {
            if ($("#provinceSelect>span").text() == v.province) {
                cities.push({"name": v.name, "id": v.id});
            }
        })
    } else {
        $("#citySelect>span").text("选择市");
        var provinceMatch = val.toString().substr(0, 2);
        var count = 0;
        cities.splice(0, cities.length);
        $.each(dataCity.city_object, function (k, v) {
            cityReady = v.id;
            var cityMatch = cityReady.toString().substr(0, 2);
            if (cityMatch == provinceMatch) {
                count++;
                cities.push({"name": v.name, "id": v.id});
            }
        })
    }
    $("#city").empty();
    $("#city").append("<option value='-1'>请选择</option>");
    $.each(cities, function (k, v) {
        var branches = "<option value='" + v.id + "'>" + v.name + "</option>";
        $("#city").append(branches);
    });
    $("#city").append("<option value='000100'>其他</option>");
}
function get_predate(obj) {
    $("#predateSelect>span").text(obj.value);
}
function load() {
    // //初始化页面
    var dataLocal = dataload("json/provs.json");
    var clientHeight = document.documentElement.clientHeight;
    console.log(clientHeight);
    // $("#nextButton").attr("disabled",false);
    $("body").css({"height": clientHeight});
    $(".footer").css({"height": clientHeight - 702 - 110 + "px"});
    code = GetQueryString("code");
    backurl = GetQueryString("backurl");
    GroupID = GetQueryString("GroupID");
    aid = GetQueryString("aid");
    an = GetQueryString("an");
    cookie_openID = $.cookie("openid_cookie");
    userInfo_reply = dataload("http://wx.gemii.cc/shakeFu/getShakeFuInfo?code=" + code + "&openid=" + cookie_openID + "&GroupID=" + GroupID + "&aid=" + aid + "&an=" + an);
    if (userInfo_reply.status == 200) {
        openID = userInfo_reply.data.openid;
        $.cookie("openid_cookie", openID, {"expires": 1});
        if (!(userInfo_reply.data.shakeFu == null)) {
            window.location.href = backurl + "?" + userInfo_reply.data.shakeFu;
        } else {

        }
    } else {
        alert("请求失败,请刷新重试");
        return false;
    }

    $("#province").append("<option value='-1'>请选择</option>");
    $.each(dataLocal.province_object, function (k, v) {
        var branches = "<option value='" + v.id + "'>" + v.name + "</option>";
        $("#province").append(branches);
    })
    $("#province").append("<option value='000000'>其他</option>");
}
$(function () {
    // $("#nextButton").get(0).addEventListener("touchstart", function () {
    //     $(".nextButton").css({"background": "#E1D247"});
    // });
    $("#nextButton").get(0).addEventListener("click", function () {
        $(".buttonBox").addClass("buttonBox_after");
        setTimeout(function () {
            $(".buttonBox").removeClass("buttonBox_after");
        }, 100);
        setTimeout(function () {
            if ($("#provinceSelect>span").text() == "" || $("#provinceSelect>span").text() == "选择省"
                || $("#citySelect>span").text() == "" || $("#citySelect>span").text() == "选择市"
                || $("#predateSelect>span").text() == "" || $("#predateSelect>span").text() == "选择预产期") {
                alert("请完成选择");
            } else if (!(/^1[34578]\d{9}$/.test($("#tel").val()))) {
                alert("您输入的手机号有误");

            } else {
                $("#nextButton").attr("disabled", true);
                userInfoArray.splice(0, userInfoArray.length);
                userInfoArray.push({"name": "province", "value": $("#provinceSelect>span").text()},
                    {"name": "city", "value": $("#citySelect>span").text()},
                    {"name": "babyBir", "value": $("#predateSelect>span").text()},
                    {"name": "telephone", "value": $("#tel").val()},
                    {"name": "openid", "value": openID},
                    {"name": "GroupID", "value": GroupID},
                    {"name": "aid", "value": aid},
                    {"name": "an", "value": an});
                // parms_infoArray = userInfoArray.concat(GetUrlParms());
                //数据上传处理
                $.ajax({
                    data: userInfoArray,
                    type: "POST",
                    url: "http://wx.gemii.cc/shakeFu/saveShakeFuInfo",
                    dataType: "json",
                    async: false,
                    beforeSend: function () {
                        $("#nextButton").attr("disabled", true);
                        $("#nextButton").val("提交中...");
                    },
                    success: function (json) {
                        var arraydata = eval(json);
                        if (arraydata.status == 200) {
                            window.location.href = backurl + "?" + arraydata.data;
                        } else if (arraydata.status == 406) {
                            alert("手机号重复使用,请确定是否有误");
                            $("#nextButton").attr("disabled", false);
                        }
                        else {
                            alert("跳转出错,请刷新重试");
                        }

                    },
                    complete: function () {
                        $("#nextButton").val("提交");
                    },
                    error: function () {
                        alert("跳转失败,请刷新重试!");
                    }
                });
            }
        }, 200);
    });
    $("#city").get(0).addEventListener("touchend", function () {
        if ($("#provinceSelect>span").text() == "" || $("#provinceSelect>span").text() == "选择省") {
            alert("先选择省");
        }
    })
});